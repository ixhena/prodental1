import React, { Fragment, useEffect, useState } from "react";
import { ProSidebar, Menu, MenuItem, SubMenu } from 'react-pro-sidebar';
import 'react-pro-sidebar/dist/css/styles.css';


const SideBar = (props) => {
    const [state, setstate] = useState({ data: "" })

    const addSomething = (props) => {
    };

    return (
        <>
            <ProSidebar id="x">
                <Menu iconShape="square" >
                    <SubMenu title="Products" >
                        <MenuItem onClick={addSomething("Implants")}>Implants
                        </MenuItem>
                        <MenuItem>Component 2</MenuItem>
                    </SubMenu>
                </Menu>
            </ProSidebar>
        </>
    )
}
export default SideBar;