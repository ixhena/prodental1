import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { signout } from '../../actions/userAction';
import { Link } from 'react-scroll';
import { useTranslation } from 'react-i18next';

const HeaderMenuPrimary = () => {
  const userSignin = useSelector((state) => state.userSignin);
  const { userInfo } = userSignin;
  const dispatch = useDispatch();
  const signoutHandler = () => {
    dispatch(signout());
  }
  const { t, i18n } = useTranslation();
  return (
    <nav className="menu-primary">
      <ul className="nav">
        <li className="nav-item">
          <a title="About Us" href={process.env.PUBLIC_URL + "/#about-us"}>
            {t('About Us.1')}
          </a>
        </li>

        <li className="nav-item">
          <a title="Services" href={process.env.PUBLIC_URL + "/#services"}>
            {t('Services.1')}
          </a>
        </li>

        <li className="nav-item">
          <a title="Prediction" href={process.env.PUBLIC_URL + "/#our-team"}>
            {t('Prediction.1')}
          </a>
        </li>

        <li className="nav-item">
          <a
            title="Offers"
            href={process.env.PUBLIC_URL + "/offers"}
          >
            {t('Offers.1')}
          </a>
        </li>

        <li
          className={
            "nav-item " +
            (window.location.pathname.includes("/news/Endodonti") ||
              window.location.pathname.includes("/product")
              ? "current-nav-item"
              : "")
          }
        >
          <a title="Products" href={process.env.PUBLIC_URL + "/news/Endodonti"}>
            {t('Products.1')}
          </a>
        </li>

        <li className="nav-item">
          <a title="Contacts" href={process.env.PUBLIC_URL + "/#contacts"}>
            {t('Contacts.1')}
          </a>
        </li>
      </ul>
    </nav>
  );
};

export default HeaderMenuPrimary;
