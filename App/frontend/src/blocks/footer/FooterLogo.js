import React from 'react';

const FooterLogo = () => {
    return (
        <div className="logo logo-primary">
            <img src="../assets/img/logo/final-logo.png" alt="Logo" />
        </div>
    );
};

export default FooterLogo;
