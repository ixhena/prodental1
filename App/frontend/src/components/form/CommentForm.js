import React, { Component, useState } from 'react';
import axios from 'axios';

function CommentForm(props) {
    const { handleCommentSubmit } = props;
    const [comment, setComment] = useState('');
    axios.post('/api/comment/save', comment).then(response => {
        console.log('response', response.comment)
    });

    return (
        <form onSubmit={event => { handleCommentSubmit(comment); }} action="" method="post" id="commentform" className="comment-form">
            <div className="form-group form-group-xs">
                <p className="comment-notes">Your email address will not be published. Required fields are marked <span className="required">*</span></p>
            </div>
            <div className="form-group form-group-xs">
                <textarea id="comment"
                    className="form-control form-lg"
                    name="comment"
                    cols="45" rows="4"
                    placeholder="Your comment here *"
                    value={comment}
                    onChange={event => setComment(event.target.value)}
                    required="required">
                </textarea>
            </div>
            <div className="form-submit">
                <input name="submit" type="submit" className="btn btn-primary" value="Post Comment" />
            </div>
        </form>
    );
}

export default CommentForm;
