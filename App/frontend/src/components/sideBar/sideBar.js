import React, { Component, useState } from 'react';
import axios from 'axios';

const sideBar = (props) => {

    return (
        <form >
            <label for="cars">Choose a car:</label>
            <select name="cars" id="cars">
                <optgroup label="Swedish Cars">
                    <option value="volvo">Volvo</option>
                    <option value="saab">Saab</option>
                </optgroup>
                <optgroup label="German Cars">
                    <option value="mercedes">Mercedes</option>
                    <option value="audi">Audi</option>
                </optgroup>
            </select>
            <br></br>
            <input type="submit" value="Submit"></input>
        </form>
    );
}

export default sideBar;
