import React from 'react';

const ButtonSearch = (props) => {
    return (
        <button type="button" onClick={props.searchButton} className="btn btn-lg btn-link border-0 p-0 min-w-auto link-no-space"><i className="fas fa-search"></i></button>
    );
};

export default ButtonSearch;
