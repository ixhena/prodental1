import React, { Fragment, useEffect, useState } from "react";
import MetaTags from "react-meta-tags";
import { useLocation, useNavigate } from 'react-router-dom';
import Header from "../blocks/header/Header";
import Footer from "../blocks/footer/Footer";
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { register } from '../actions/userAction';
import Loading from "../blocks/loading/Loading";
import MessageBox from '../components/MessageBox';

export default function RegisterScreen() {
  const [name, setName] = useState('');
  const [nipt, setNipt] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  let location = useLocation();
  const navigate = useNavigate();
  const redirect = location.search
    ? location.search.split('=')[1]
    : '/';

  const userRegister = useSelector((state) => state.userRegister);
  const { userInfo, loading, error } = userRegister;
  let validNipt = new RegExp(
    '^[a-zA-Z0][Z0-9.-][Z0-9.-][Z0-9.-][Z0-9.-][Z0-9.-][Z0-9.-][Z0-9.-][Z0-9.-][a-zA-Z0]$'
  );

  const dispatch = useDispatch();
  const submitHandler = (e) => {
    e.preventDefault();
    if (password !== confirmPassword) {
      setErrorMessage('Password and confirm password are not match');
    } else {
      if (validNipt.test(nipt) == true) {
        dispatch(register(name, nipt, email, password));
      } else if (validNipt.test(nipt) == false) {
        setErrorMessage('Invalid NIPT');
      }
    }
  };
  useEffect(() => {
    if (userInfo) {
      navigate(redirect);
    }
  }, [navigate, redirect, userInfo]);
  return (
    <Fragment>
      <MetaTags>
        <meta charSet="UTF-8" />
        <title>
          News single post | Hosco - Dentist & Medical React JS Template
        </title>

        <meta httpEquiv="x-ua-compatible" content="ie=edge" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1"
        />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="robots" content="index, follow, noodp" />
        <meta name="googlebot" content="index, follow" />
        <meta name="google" content="notranslate" />
        <meta name="format-detection" content="telephone=no" />
      </MetaTags>

      <Loading />

      <Header />
      <div>
        <form className="form" onSubmit={submitHandler}>
          <div>
            <h1>Create Account</h1>
          </div>
          {loading && <Loading></Loading>}
          {error ? <MessageBox variant="danger">{'This email has already an account!'}</MessageBox> : <MessageBox variant="danger">{errorMessage}</MessageBox>}
          <div>
            <label htmlFor="name">Name</label>
            <input
              type="text"
              id="name"
              placeholder="Enter name"
              required
              onChange={(e) => setName(e.target.value)}
            ></input>
          </div>
          <div>
            <label htmlFor="email">Email address</label>
            <input
              type="email"
              id="email"
              placeholder="Enter email"
              required
              onChange={(e) => setEmail(e.target.value)}
            ></input>
          </div>
          <div>
            <label htmlFor="nipt">NIPT</label>
            <input
              type="text"
              id="nipt"
              placeholder="Enter NIPT"
              required
              onChange={(e) => setNipt(e.target.value)}
            ></input>
          </div>
          <div>
            <label htmlFor="password">Password</label>
            <input
              type="password"
              id="password"
              placeholder="Enter password"
              required
              onChange={(e) => setPassword(e.target.value)}
            ></input>
          </div>
          <div>
            <label htmlFor="confirmPassword">Confirm Password</label>
            <input
              type="password"
              id="confirmPassword"
              placeholder="Enter confirm password"
              required
              onChange={(e) => setConfirmPassword(e.target.value)}
            ></input>
          </div>
          <div>
            <label />
            <button className="primary" type="submit">
              Register
            </button>
          </div>
          <div>
            <label />
            <div>
              Already have an account?{' '}
              <Link to={`/signin?redirect=${redirect}`}>Sign-In</Link>
            </div>
          </div>
        </form>
        <Footer />
      </div>
    </Fragment>
  );
}