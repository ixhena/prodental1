import React, { Fragment, useEffect, useState } from "react";
import MetaTags from "react-meta-tags";
import Header from "../blocks/header/Header";
import Footer from "../blocks/footer/Footer";
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import '../index.css';
import { useLocation, useNavigate } from 'react-router-dom';
import Loading from "../blocks/loading/Loading";
import MessageBox from '../components/MessageBox';
import { reset } from '../actions/userAction';

const ForgetPassword = (props) => {
    const [email, setEmail] = useState('');
    document.body.classList.add("signin");
    let location = useLocation();
    const redirect = location.search
        ? location.search.split('=')[1]
        : '/';
    const userSignin = useSelector((state) => state.userSignin);
    const { userInfo, error } = userSignin;
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const submitHandler = (e) => {
        e.preventDefault();
        console.log(email)
        dispatch(reset(email));
        window.location.reload();
    }
    useEffect(() => {
        if (userInfo) {
            navigate("/");
        }
    }, [navigate, redirect, userInfo]);
    return (
        <div>
            <Header />
            <form className="form" onSubmit={submitHandler}>
                <div>
                    <h1>Reset</h1>
                </div>
                {<Loading></Loading>}
                {<MessageBox variant="danger">{error}</MessageBox>}
                <div>
                    <label htmlFor="email">Email address</label>
                    <input
                        type="email"
                        id="email"
                        placeholder="Enter email"
                        required
                        onChange={(e) => setEmail(e.target.value)}
                    ></input>
                </div>
                <div>
                    <label />
                    <button className="primary" type="submit">
                        Submit
                    </button>
                </div>
            </form>
            <Footer />
        </div>
    );
};
export default ForgetPassword;
