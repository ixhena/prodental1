import React, { Fragment, useEffect, useState } from "react";
import MetaTags from "react-meta-tags";
import Loading from "../blocks/loading/Loading";
import Header from "../blocks/header/Header";
import Footer from "../blocks/footer/Footer";
import PageTitleNews from "../blocks/page-title/PageTitleNews";
import NewsMeta from "../blocks/news/NewsMeta";
import NewsDescription from "../blocks/news/NewsDescription";
import NewsTags from "../blocks/news/NewsTags";
import { useDispatch, useSelector } from "react-redux";
import MessageBox from "../components/MessageBox";
import { detailsProduct, listProducts } from "../actions/productActions";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLock } from "@fortawesome/free-solid-svg-icons";
import { useTranslation } from 'react-i18next';
import { useLocation, useNavigate } from 'react-router-dom';

const NewsSinglePost = () => {
  document.body.classList.add("single-post");
  const dispatch = useDispatch();
  const navigate = useNavigate();
  let location = useLocation();
  const [qty, setQty] = useState(1);
  const productDetails = useSelector((state) => state.productDetails);
  const { loading, error, product } = productDetails;
  const cart = useSelector((state) => state.cart);
  const { cartItems } = cart;
  const userSignin = useSelector((state) => state.userSignin);
  const { userInfo } = userSignin;
  var str = location.pathname;
  var n = str.lastIndexOf("/");
  var result = str.substring(n + 1);
  var id;
  var name;
  var price;
  var description;
  var countInStock;
  var image;
  var discountPrice;
  var endDate;
  var startDate;
  var discount;
  var isOffer;
  var isUserOffer;
  var [date, setDate] = useState(new Date());
  const redirect = location.search
    ? location.search.split("=")[1]
    : "/";
  if (product !== undefined) {
    if (result == product._id) {
      id = product._id;
      name = product.name;
      price = product.price;
      description = product.description;
      countInStock = product.countInStock;
      image = product.image;
      discount = product.discount;
      discountPrice = product.discountPrice;
      endDate = product.endDate;
      startDate = product.startDate;
      isOffer = product.isOffer;
      isUserOffer = product.isUserOffer;
    }
  }
  console.log(isOffer)
  useEffect(() => {
    dispatch(detailsProduct(result));
  }, [dispatch, result]);

  const addToCartHandler = (qty) => {
    navigate(`/cart/${product._id}?qty=${qty}`);
  };
  let stock = [];
  for (var i = 0; i <= countInStock; i++) {
    stock.push(i);
  }
  var tzoffsetdate = (new Date(date)).getTimezoneOffset() * 60000;
  var dateInitial = (new Date(date - tzoffsetdate)).toISOString().slice(0, -1);
  dateInitial = dateInitial.substr(0, dateInitial.indexOf('T'));
  if (startDate != undefined) {
    startDate = new Date(startDate);
    var tzoffsetdate = (new Date(startDate)).getTimezoneOffset() * 60000;
    var startDate = (new Date(startDate - tzoffsetdate)).toISOString().slice(0, -1);
    startDate = startDate.substr(0, startDate.indexOf('T'));
    endDate = new Date(endDate);
    var tzoffsetdate = (new Date(endDate)).getTimezoneOffset() * 60000;
    var endDate = (new Date(endDate - tzoffsetdate)).toISOString().slice(0, -1);
    endDate = endDate.substr(0, endDate.indexOf('T'));
  }
  const signinFirst = () => {
    navigate(`/signin?redirect=/product/${id}`);
  };
  const { t, i18n } = useTranslation();

  return (
    <div>
      {loading ? (
        <Loading></Loading>
      ) : error ? (
        <MessageBox variant="danger">{error}</MessageBox>
      ) : (
        <Fragment>
          <MetaTags>
            <meta charSet="UTF-8" />
            <meta httpEquiv="x-ua-compatible" content="ie=edge" />
            <meta
              name="viewport"
              content="width=device-width, initial-scale=1"
            />
            <meta name="description" content="" />
            <meta name="keywords" content="" />
            <meta name="robots" content="index, follow, noodp" />
            <meta name="googlebot" content="index, follow" />
            <meta name="google" content="notranslate" />
            <meta name="format-detection" content="telephone=no" />
          </MetaTags>

          <Loading />

          <Header />

          <main id="main" className="site-main">
            {/* <PageTitleNews /> */}

            <div id="page-content" className="spacer p-top-xl">
              <div className="wrapper">
                <div className="content">
                  <div className="row ">
                    <div class="columnSingleItem" style={{ width: "50%" }}>
                      {endDate != undefined ?
                        dateInitial <= endDate && discount > 0 && isOffer == false && isUserOffer == false ?
                          <div className="price" style={{ marginLeft: "9%", position: "absolute", zIndex: "1", marginTop: "7%", fontSize: "60px", fontFamily: "cursive", color: "#48D1CC" }}>-{discount}<img src="/assets/img/placeholder/discount.png" style={{ width: "100px" }}></img></div> : <></> : <></>}
                      <img src={image} alt={name} style={{ width: "100%", height: "90%", objectFit: "contain" }} />
                    </div>
                    <div class="columnSingleItem" style={{ width: "50%" }}>
                      <div
                        className="card card-body"
                        style={{
                          backgroundColor: "aliceblue",
                          borderColor: "bisque",
                          width: "89%",
                          marginLeft: "17%"
                        }}
                      >
                        <ul>
                          <li>
                            <h1 style={{ color: "rgb(0, 110, 127)" }}>{name}</h1>
                          </li>
                          <li>
                            <p style={{ color: "rgb(0, 110, 127)" }}>{t('Description.1')}:</p>
                            < p style={{ "whiteSpace": "pre-wrap" }}>{description}</p>
                          </li>
                          <li>
                            <div className="row">
                              {userInfo !== null ?
                                dateInitial <= endDate && discount > 0 && isOffer == false && isUserOffer == false ?
                                  <ul>
                                    <div className="price" style={{ color: "rgb(0, 110, 127)" }}>{discountPrice}ALL</div>
                                    <s className="price" style={{ color: "lightgrey", fontSize: "27px" }}>{price}ALL</s>
                                  </ul> : <div className="price">{price}ALL</div>
                                : (
                                  <a onClick={() => signinFirst()}>
                                    <FontAwesomeIcon
                                      icon={faLock}
                                    ></FontAwesomeIcon>
                                  </a>
                                )}
                            </div>
                          </li>
                          <li>
                            <div className="row">
                              <div style={{ color: "rgb(0, 110, 127)" }}>Status</div>
                              <div>
                                {countInStock > 0 ? (
                                  <span className="success">{t('In Stock.1')}</span>
                                ) : (
                                  <span className="danger">
                                    {t('Unavailable.1')}
                                  </span>
                                )}
                              </div>
                            </div>
                          </li>

                          {countInStock > 0 && (
                            <>
                              <li>
                                <div className="row">
                                  <div style={{ color: "rgb(0, 110, 127)" }}>Qty</div>
                                  <div>
                                    <select
                                      value={qty}
                                      onChange={(e) =>
                                        setQty(e.target.value)
                                      }
                                    >
                                      {stock.map((x) => (
                                        <option key={x + 1} value={x + 1}>
                                          {x + 1}
                                        </option>
                                      ))}
                                    </select>
                                  </div>
                                </div>
                              </li>
                            </>
                          )}
                          <li>
                            <button
                              onClick={() => addToCartHandler(qty)}
                              className="primary block"
                            >
                              {t('AddtoCart.1')}
                            </button>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </main>
          <div style={{ marginTop: "150px" }}>
            <Footer></Footer>
          </div>
        </Fragment>
      )
      }
    </div >
  );
};

export default NewsSinglePost;
