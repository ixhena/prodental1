import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { deleteUser, listUsers, reset } from '../actions/userAction';
import { listOffers, deleteOffers, updateOffer } from '../actions/offersAction';
import LoadingBox from '../components/loadmore/LoadMore';
import MessageBox from '../components/MessageBox';
import { USER_DETAILS_RESET } from '../constants/userConstants';
import Header from "../blocks/header/Header";
import Footer from '../blocks/footer/Footer';
import { Link } from "react-router-dom";
import nodemailer from "nodemailer";
import OrderHistoryScreen from './OrderHistoryScreen';
import emailjs from 'emailjs-com';
import { useLocation, useNavigate } from 'react-router-dom';

export default function UserListOffers() {
  const userList = useSelector((state) => state.userList);
  const navigate = useNavigate();
  let location = useLocation();
  const { loading, error, users } = userList;
  const [approved, setApproved] = useState(false);
  const offerList = useSelector((state) => state.offerList);
  const { offers } = offerList;
  const userDelete = useSelector((state) => state.userDelete);
  const {
    loading: loadingDelete,
    error: errorDelete,
    success: successDelete,
  } = userDelete;
  const redirect = location.search
    ? location
    : '/userlist';
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(listUsers());
    dispatch(listOffers())
    dispatch({
      type: USER_DETAILS_RESET,
    });
  }, [dispatch, redirect, successDelete]);
  const deleteOffer = (id, description, price, approved) => {
    if (window.confirm('Are you sure?')) {
      dispatch(deleteOffers(id));
      window.location.reload();
    }
  };

  const [result, showResult] = useState(false);

  const approveOffer = (name, email, price, description, approved, id, currency) => {
    if (approved == false) {
      approved = true;
      dispatch(updateOffer({ id, description, price, approved, currency }));
    }
    emailjs.send("service_cbpnylm", "template_c1qeh2t", {
      name: name,
      email: email,
      price: price,
      description: description,
      currency: currency,
    }, "user_GYW5t8jrx2ZCxAxEoHbnw")
      .then((result) => {
        console.log(result.text);
      }, (error) => {
        console.log(error.text);
      });
    reset()
    showResult(true);
    setTimeout(() => {
      showResult(false);
    })
  }

  return (
    <div>
      <Header />
      <h1>Users</h1>
      {loadingDelete && <LoadingBox></LoadingBox>}
      {errorDelete && <MessageBox variant="danger">{errorDelete}</MessageBox>}
      {successDelete && (
        <MessageBox variant="success">User Deleted Successfully</MessageBox>
      )}
      {loading ? (
        <LoadingBox></LoadingBox>
      ) : error ? (
        <MessageBox variant="danger">{error}</MessageBox>
      ) : (
        <div style={{ overflowX: "auto" }}>
          <table className="table">
            <thead>
              <tr>
                <th>ID</th>
                <th>NAME</th>
                <th>Email</th>
                <th>Description</th>
                <th>Price</th>
                <th>currency</th>
                <th>Status</th>
                <th>ACTIONS</th>
              </tr>
            </thead>
            <tbody>
              {offers !== undefined ?
                offers.map((offer) => (
                  <tr key={offer._id}>
                    <td>{offer._id}</td>
                    {users.map(user => (
                      user._id == offer.user ?
                        <><td id="name">{user.name}</td>
                          <td id="email">{user.email}</td></> : <></>
                    ))}
                    <td id="description">{offer.description}</td>
                    <td id="price">{offer.price}</td>
                    <td id="price">{offer.currency}</td>
                    {offer.approved ?
                      <td >Approved</td> : <td id="price">Not Approved</td>
                    }
                    {users.map(user => (
                      user._id == offer.user ?
                        <td>
                          <button
                            type="button"
                            className="small"
                            onClick={(e) => approveOffer(user.name, user.email, offer.price, offer.description, offer.approved, offer._id, offer.currency)}
                          >
                            Approve
                          </button>
                          <button
                            type="button"
                            className="small"
                            onClick={() => deleteOffer(offer._id, offer.description, offer.price, offer.approved)}
                          >
                            Delete
                          </button>

                        </td>
                        : <></>))}
                  </tr>
                )) : <></>}
            </tbody>
          </table>
        </div>
      )}
      <div style={{ marginTop: "150px" }}>
        <Footer></Footer>
      </div>    </div>
  );
}
