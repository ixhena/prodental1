import React, { Fragment, useEffect, useState } from "react";
import MetaTags from "react-meta-tags";
import Loading from "../blocks/loading/Loading";
import Header from "../blocks/header/Header";
import Footer from "../blocks/footer/Footer";
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { createOrder } from '../actions/orderAction';
import CheckoutSteps from '../components/CheckoutSteps';
import { ORDER_CREATE_RESET } from '../constants/orderConstants';
import LoadingBox from '../components/loadmore/LoadMore';
import MessageBox from '../components/MessageBox';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom'

export default function PlaceOrderScreen() {
  const cart = useSelector((state) => state.cart);
  const navigate = useNavigate();
  var [date, setDate] = useState(new Date());
  var tzoffsetdate = (new Date(date)).getTimezoneOffset() * 60000;
  var dateInitial = (new Date(date - tzoffsetdate)).toISOString().slice(0, -1);
  dateInitial = dateInitial.substr(0, dateInitial.indexOf('T'));
  var startDate;
  var endDate;
  Object.keys(cart.cartItems).map((id) => {
    startDate = cart.cartItems[id].startDate;
    endDate = cart.cartItems[id].endDate;
  })
  if (startDate != undefined || endDate != undefined) {
    startDate = startDate.substr(0, startDate.indexOf('T'));
    endDate = endDate.substr(0, endDate.indexOf('T'));
  }
  const orderCreate = useSelector((state) => state.orderCreate);
  const { loading, success, error, order } = orderCreate;
  const toPrice = (num) => Number(num.toFixed(2)); // 5.123 => "5.12" => 5.12
  cart.itemsPrice = toPrice(
    cart.cartItems.reduce((a, c) =>
      a + (c.endDate > dateInitial ? c.discountPrice || c.price : c.price) * c.qty, 0)
  );
  cart.shippingPrice = cart.itemsPrice >= 5000 || cart.shippingAddress.city == "Tirane" || cart.shippingAddress.city == "Tirana" || cart.shippingAddress.city == "tirane" || cart.shippingAddress.city == "tirana" ? toPrice(0) : toPrice(200);
  // cart.taxPrice = toPrice(0.15 * cart.itemsPrice);
  // cart.totalPrice = cart.itemsPrice + cart.shippingPrice + cart.taxPrice;
  cart.totalPrice = cart.itemsPrice

  const dispatch = useDispatch();
  const placeOrderHandler = () => {
    dispatch(createOrder({ ...cart, orderItems: cart.cartItems }));
  };
  useEffect(() => {
    if (success) {
      navigate(`/order/${order._id}`);
      dispatch({ type: ORDER_CREATE_RESET });
    }
  }, [dispatch, order, navigate, success]);
  const { t, i18n } = useTranslation();

  return (
    <Fragment>
      <MetaTags>
        <meta charSet="UTF-8" />
        <title>
          News single post | Hosco - Dentist & Medical React JS Template
        </title>
        <meta httpEquiv="x-ua-compatible" content="ie=edge" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1"
        />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="robots" content="index, follow, noodp" />
        <meta name="googlebot" content="index, follow" />
        <meta name="google" content="notranslate" />
        <meta name="format-detection" content="telephone=no" />
      </MetaTags>

      <Loading />

      <Header />
      <div>
        <CheckoutSteps step1 step2 step3 step4></CheckoutSteps>
        <div className="row top">
          <div className="col-2">
            <ul>
              <li>
                <div className="card card-body">
                  <h2>{t("Shipping.1")}</h2>
                  <p>
                    <strong>{t("Name.1")}:</strong> {cart.shippingAddress.fullName} <br />
                    <strong>{t("Address.1")}: </strong> {cart.shippingAddress.address},
                    {cart.shippingAddress.city}, {cart.shippingAddress.postalCode}
                    ,{cart.shippingAddress.country}
                  </p>
                </div>
              </li>
              <li>
                <div className="card card-body">
                  <h2>{t("Order Items.1")}</h2>
                  <ul>
                    {cart.cartItems.map((item) => (
                      <li key={item.product}>
                        <div className="row">
                          <div>
                            <img
                              src={item.image}
                              alt={item.name}
                              className="small"
                            ></img>
                          </div>
                          <div className="min-30">
                            <Link to={`/product/${item.product}`}>
                              {item.name}
                            </Link>
                          </div>
                          <div>{item.endDate >= dateInitial ?
                            <div>
                              {item.qty} x {item.discountPrice || item.price} = {item.qty * item.discountPrice || item.price}ALL
                            </div>
                            :
                            <div>
                              {item.qty} x {item.price} = {item.qty * item.price}ALL
                            </div>}
                          </div>
                        </div>
                      </li>
                    ))}
                  </ul>
                </div>
              </li>
            </ul>
          </div>
          <div className="col-1">
            <div className="card card-body">
              <ul>
                <li>
                  <h2>{t("Order Summary.1")}</h2>
                </li>
                <li>
                  <div className="row">
                    <div>{t("items.1")}</div>
                    <div>
                      <div>{cart.itemsPrice.toFixed(2)}ALL</div>
                    </div>
                  </div>
                </li>
                <li>
                  <div className="row">
                    <div>{t("Shipping.1")}</div>
                    <div>{cart.shippingPrice.toFixed(2)}ALL</div>
                  </div>
                </li>
                <li>
                  <div className="row">
                    <div>
                      <strong> {t("Order Total.1")}</strong>
                    </div>
                    <div>
                      <strong>{(cart.itemsPrice + cart.shippingPrice).toFixed(2)}ALL</strong>
                    </div>
                  </div>
                </li>
                {/* <li>
                      <div className="row">
                        <div>Tax</div>
                        <div>{cart.taxPrice.toFixed(2)}ALL</div>
                      </div>
                    </li>  */}
                <li>
                  <div className="row">
                    <div style={{ width: "100%" }}>
                      <button
                        type="button"
                        onClick={placeOrderHandler}
                        className="primary block"
                        disabled={cart.cartItems.length === 0}
                      >
                        {t("Place Order.1")}
                      </button>
                    </div>
                  </div>
                </li>
                {loading && <LoadingBox></LoadingBox>}
                {error && <MessageBox variant="danger">{error}</MessageBox>}
              </ul>
            </div>
          </div>
        </div>
        <div style={{ marginTop: "150px" }}>
          <Footer></Footer>
        </div>
      </div>
    </Fragment>
  );
}