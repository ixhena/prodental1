import React, { Fragment, useEffect } from "react";
import MetaTags from "react-meta-tags";
import Loading from "../blocks/loading/Loading";
import Header from "../blocks/header/Header";
import Footer from "../blocks/footer/Footer";
import { ProSidebar, Menu, MenuItem, SubMenu } from "react-pro-sidebar";
import "react-pro-sidebar/dist/css/styles.css";
import SideBar from "../blocks/sidebar/SideBar";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Link } from "react-router-dom";
import { Redirect } from "react-router-dom";
import PageTitleNews from "../blocks/page-title/PageTitleNews";
import Widget from "../blocks/widget/Widget";
import LoadMore from "../components/loadmore/LoadMore";
import { useDispatch, useSelector } from "react-redux";
import NewsItemsData from "../data/news/newsItems";
import MessageBox from "../components/MessageBox";
import { listProducts } from "../actions/productActions";
import NewsSinglePost from "./NewsSinglePost";
import { useState } from "react";
import App from "../App";
import { Backdrop } from "@material-ui/core";
import BackgroundColor from "../blocks/ui/utilities/colors/children/BackgroundColor";
import { Button, Card, CloseButton, Navbar, Nav, Container } from "react-bootstrap";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Badge from "@material-ui/core/Badge";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import Icon from '@mui/material/Icon';
import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Remove";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlusCircle, faMinusCircle } from '@fortawesome/free-solid-svg-icons'
import { createOffers } from '../actions/offersAction';
import { updateProduct } from '../actions/productActions';
import { useTranslation } from 'react-i18next';
import { useNavigate, useLocation, useParams } from 'react-router-dom';

const OfferScreen = () => {
  document.body.classList.add("blog");
  let { params } = useParams();
  let categ = "";
  if (params !== undefined) {
    categ = params.res;
  }
  const [price, setPrice] = useState('');
  const [description, setDescription] = useState('');
  const [productcateg, setProductcateg] = useState("");
  const [currency, setCurrency] = useState('');
  const [itemCount, setItemCount] = useState(1);
  const [cartItems, setCartItems] = useState([]);
  const dispatch = useDispatch();
  const productList = useSelector((state) => state.productList);
  const { loading, error, products } = productList;
  const userSignin = useSelector((state) => state.userSignin);
  const { userInfo } = userSignin;
  const [inputValue, setInputValue] = useState("");
  const userDetails = useSelector((state) => state.userDetails);
  const { user } = userDetails;
  const offer = useSelector((state) => state.offersCreate);
  const { offers } = offer;
  const { t, i18n } = useTranslation();
  const navigate = useNavigate();

  if (offers !== undefined) {
    console.log(offers.offers)
  }
  const [date, setDate] = useState(new Date());
  useEffect(() => {
    dispatch(listProducts());
  }, []);
  if (categ !== "") {
    App(categ);
  }

  const onAdd = (product) => {
    const exist = cartItems.find(x => x._id === product._id);
    if (exist) {
      setCartItems(cartItems.map((x) => x._id === product._id ? {
        ...exist, qty: exist.qty + 1
      } : x
      ))
    } else {
      setCartItems([...cartItems, { ...product, qty: 1 }])
    }
  };
  const onRemove = (product) => {
    const exist = cartItems.find(x => x._id === product._id);
    if (exist) {
      setCartItems(cartItems.map((x) => x._id === product._id ?
        exist.qty >= 2 ? ({
          ...exist, qty: exist.qty - 1
        }) : x : x
      ))
    } else {
      setCartItems([...cartItems, { ...product, qty: 1 }])
    }
  };
  var badgeContent = localStorage.getItem('cartItems');
  const addToCartHandler = (product) => {
    navigate(`/cart/${product._id}?qty=${product.qty}`);
    navigate(-1);
  };
  var tzoffsetdate = (new Date(date)).getTimezoneOffset() * 60000;
  var dateInitial = (new Date(date - tzoffsetdate)).toISOString().slice(0, -1);
  dateInitial = dateInitial.substr(0, dateInitial.indexOf('T'));
  const openNav = (e) => {
    e.preventDefault();
    document.getElementById("mySidenav").style.width = "250px";
  }

  function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }
  const userOffer = () => {
    dispatch(createOffers({
      description,
      price,
      currency
    }));
    window.location.reload();
  }
  return (

    <div>
      {loading ? (
        <Loading></Loading>
      ) : error ? (
        <MessageBox variant="danger">{error}</MessageBox>
      ) : (
        <Fragment>
          <MetaTags>
            <meta charSet="UTF-8" />
            <title>News | Hosco - Dentist & Medical React JS Template</title>

            <meta httpEquiv="x-ua-compatible" content="ie=edge" />
            <meta
              name="viewport"
              content="width=device-width, initial-scale=1"
            />
            <meta name="description" content="" />
            <meta name="keywords" content="" />
            <meta name="robots" content="index, follow, noodp" />
            <meta name="googlebot" content="index, follow" />
            <meta name="google" content="notranslate" />
            <meta name="format-detection" content="telephone=no" />
          </MetaTags>
          <Loading />
          <Header />
          <Navbar bg="light" variant="light">
            <Container>
              <Navbar.Brand >
                <Nav.Link href={
                  process.env.PUBLIC_URL + "/news/" + productcateg
                } onClick={() => setProductcateg("Endodonti")}
                >Endodonti</Nav.Link>
                <Nav.Link href={
                  process.env.PUBLIC_URL + "/news/" + productcateg
                } onClick={(e) =>
                  setProductcateg("Dentistry Restorative")
                }>Dentistry Restorative</Nav.Link>
                <Nav.Link href={
                  process.env.PUBLIC_URL + "/news/" + productcateg
                } onClick={() =>
                  setProductcateg("Cemente dhe shtresa izoluese")
                }>Cemente dhe shtresa izoluese</Nav.Link>
                <Nav.Link href={
                  process.env.PUBLIC_URL + "/news/" + productcateg
                } onClick={() => setProductcateg("Materiale Mase")}>Materiale Mase</Nav.Link>
                <Nav.Link href={
                  process.env.PUBLIC_URL + "/news/" + productcateg
                } onClick={() => setProductcateg("Profilaksi")}>Profilaksi</Nav.Link>
                <Nav.Link href={
                  process.env.PUBLIC_URL + "/news/" + productcateg
                } onClick={() => setProductcateg("Te tjera")}>{t('Other.1')}</Nav.Link>
                <Nav.Link href={
                  process.env.PUBLIC_URL + "/" + productcateg
                } onClick={() => setProductcateg("Offers")}>{t('Offers.1')}</Nav.Link>
                <Nav.Link>
                  <Badge color="secondary" badgeContent={JSON.parse(badgeContent) !== null ? JSON.parse(badgeContent).length : 0}>
                    <ShoppingCartIcon onClick={() => navigate(`/cart`)}>
                      {" "}
                    </ShoppingCartIcon>
                  </Badge>
                </Nav.Link>
                {userInfo !== null ?
                  <Nav.Link
                    className="openNav" onClick={(e) => openNav(e)}>&#9776; +{t('Offers.1')}</Nav.Link> :
                  <></>
                }
              </Navbar.Brand>
            </Container>
          </Navbar>
          <main id="main" className="site-main">
            <div id="page-content" className="spacer p-top-xl">
              <div className="wrapper">
                <div className="content">
                  <div id="blog">
                    <div className="row gutter-width-md">
                      <div className="col-sm-6 col-sm-6 col-sm-6 col-sm-12">
                        <div className="row gutter-width-md with-pb-lg">
                          <div id="mySidenav" class="sidenav">
                            <form className="form" id="offerForm" onSubmit={userOffer} style={{ color: "#006e7f" }}>
                              <div>
                                <h1 style={{ color: "#006e7f" }}>{t("Suggest Your Offer.1")}!</h1>
                              </div>
                              <div>
                                <label htmlFor="price" style={{ color: "#006e7f" }}>{t("Price.1")}</label>
                                <input
                                  id="price"
                                  type="number"
                                  placeholder={t("Enter price.1")}
                                  value={price}
                                  onChange={(e) => setPrice(e.target.value)}
                                  required
                                ></input>
                              </div>
                              <div>
                                <label htmlFor="currency" style={{ color: "#006e7f" }}>{t("Currency.1")}</label>
                                <select name="currency" id="currency" onChange={(e) => setCurrency(e.target.value)}>
                                  <option >Currency</option>
                                  <option value="Lek">ALL</option>
                                  <option value="Euro">EUR</option>
                                  <option value="Dollar">US</option>
                                </select>
                              </div>
                              <div>
                                <label htmlFor="description" style={{ color: "#006e7f" }}>{t("Description.1")}</label>
                                <textarea
                                  id="description"
                                  rows="3"
                                  type="text"
                                  placeholder={t("Enter description.1")}
                                  value={description}
                                  onChange={(e) => setDescription(e.target.value)}
                                  required
                                ></textarea>
                              </div>
                              <div>
                                <label></label>
                                <button className="primary" type="submit" style={{ backgroundColor: "#006e7f", color: "white" }}>
                                  {t("Update.1")}
                                </button>
                              </div>

                            </form>
                            <a class="closebtn" onClick={closeNav}>&times;</a>
                          </div>
                          {Object.keys(products).map((key) => {
                            if (userInfo !== null && userInfo !== undefined) {
                              if ((products[key].isOffer === true || (products[key].isUserOffer === true && userInfo.name == products[key].category)) && products[key].countInStock > 0 && products[key].endDate != "") {
                                if (dateInitial <= products[key].endDate.substr(0, products[key].endDate.indexOf('T'))) {
                                  return (
                                    <>
                                      <Card style={{ width: "18rem" }}>
                                        <Card.Img
                                          variant="top"
                                          src={products[key].image}
                                          alt={products[key].name}
                                        />
                                        <Card.Body>
                                          <a
                                            href={
                                              process.env.PUBLIC_URL +
                                              "/product" +
                                              "/" +
                                              products[key]._id
                                            }
                                          >
                                            <Card.Title>
                                              {products[key].name}
                                            </Card.Title>
                                          </a>

                                        </Card.Body>
                                        <div className="title">
                                          <div class="product-item-inner">
                                            <div class="product actions product-item-actions ">
                                              <div class="actions-primary ">
                                                <form
                                                  data-role="tocart-form"
                                                  action=""
                                                >
                                                  <input
                                                    type="hidden"
                                                    name="product"
                                                    value="6450"
                                                  ></input>
                                                  <input
                                                    type="hidden"
                                                    name="uenc"
                                                    value=""
                                                  ></input>
                                                  <div class="qty-box">
                                                    <a class="qtyminus">
                                                      <RemoveIcon
                                                        fontSize="small"
                                                        style={{
                                                          color: "#006e7f",
                                                        }}
                                                        onClick={() =>
                                                          onRemove(products[key])
                                                        }
                                                      />
                                                    </a>

                                                    {Object.keys(cartItems).map((y) => {
                                                      if (cartItems[y] !== undefined && cartItems[y]._id == products[key]._id) {
                                                        return (
                                                          <>
                                                            <div
                                                              title="Qtà"
                                                              class="input-text qty"
                                                              data-validate="null"
                                                              style={{ border: "none" }}
                                                            >
                                                              {
                                                                cartItems[y].qty
                                                              }
                                                            </div>

                                                          </>
                                                        )
                                                      }
                                                    })}
                                                    <a class="qtyplus">
                                                      <AddIcon
                                                        fontSize="small"
                                                        style={{
                                                          color: "#006e7f",
                                                        }}
                                                        onClick={() =>
                                                          onAdd(products[key])
                                                        }
                                                      />
                                                    </a>
                                                  </div>

                                                  {Object.keys(cartItems).map((y) => {
                                                    if (cartItems[y] !== undefined && cartItems[y]._id == products[key]._id) {
                                                      return (
                                                        <>
                                                          <button
                                                            onClick={() => addToCartHandler(cartItems[y])}
                                                            type="submit"
                                                            title="Aggiungi al carrello"
                                                            class="action tocart primary"
                                                          >
                                                            <i class="fa fa-shopping-cart icon"></i>
                                                          </button>
                                                        </>
                                                      )
                                                    }
                                                  })}
                                                </form>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </Card>
                                    </>
                                  );
                                }
                              }
                            }

                          })}
                        </div>
                        <LoadMore />
                      </div>

                      <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                        {/* <Widget /> */}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </main>
          <Footer />
        </Fragment>
      )
      }
    </div >
  );
};

export default OfferScreen;
